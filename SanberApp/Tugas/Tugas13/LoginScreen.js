import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  TextInput,
  Button,
} from 'react-native';


export default class LoginScreen extends Component {
  render() {
    return(
      <View style={styles.container}>
        <View style={styles.posLogo}>
          <Image
          source={require('./images/logo.png')}
          />
          <Text style={styles.posReg}>Register</Text>
        </View>
        <View style={styles.posmainVi}>
          <View>
            <Text>Username</Text>
            <TextInput style={styles.styBoxtext} />
          </View>
          <View>
            <Text>Email</Text>
            <TextInput style={styles.styBoxtext} />
          </View>
          <View>
            <Text>Password</Text>
            <TextInput style={styles.styBoxtext} />
          </View>
          <View>
            <Text>Ulangi Password</Text>
            <TextInput style={styles.styBoxtext} />
          </View>
        </View>
        <View style={styles.posmainButt}>
          <TouchableOpacity style={styles.poschilButt1}>
           <Text style={{color:'#ffffff'}}>Daftar</Text>
          </TouchableOpacity>
          <Text style={styles.textines}>Atau</Text>
          <TouchableOpacity style={styles.poschilButt2}>
           <Text style={{color:'#ffffff'}}>Masuk</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container:{
    flex: 1
  },
  posImg:{
    alignItems:'center',
    justifyContent: 'center',
    marginTop: 100,
  },
  posLogo:{
    flex: 1,
    marginTop: 75,
  },
  posReg:{
    textAlign:'center',
    fontWeight:'normal',
    fontSize:26,
    marginTop: 5
  },
  posmainVi:{
    paddingHorizontal:30,
    flex: 2,
    justifyContent:'space-around'
  },
  styBoxtext:{
    borderColor:'black',
    borderWidth:1,
  },
  posmainButt:{
    justifyContent:'space-around',
    flex: 1
  },
  poschilButt1:{
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems:'center',
    marginTop:15,
    backgroundColor:'#007AFF',
    paddingVertical:10,
    marginHorizontal:125,
    borderRadius:50
  },
  poschilButt2:{
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems:'center',
    marginBottom: 15,
    backgroundColor:'#2196F3',
    paddingVertical:10,
    marginHorizontal:125,
    borderRadius:50
  },
  textines:{
    textAlign:'center',
    color:'#2196F3'
  }
});
