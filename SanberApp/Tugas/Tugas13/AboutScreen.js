import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  TextInput,
  Button,
} from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';


export default class AboutScreen extends Component {
  render() {
    return(
      <View style={styles.container}>
        <View style={styles.bodyup}>
          <Text style={styles.textup}>Tentang Saya</Text>
          <MaterialCommunityIcons name="account-circle" size={200} color="black" style={{paddingTop:5}} />
          <Text style={styles.textname}>Muklhis Hanafi</Text>
          <Text style={styles.textabout}>React Native Developer</Text>
        </View>
        <View style={styles.bodymid}>
          <Text>Portofolio</Text>
          <View style={styles.giticon}>
            <View>
              <MaterialCommunityIcons name="gitlab" size={50} color="#3EC6FF" />
              <Text>@muklhis</Text>
            </View>
            <View>
              <AntDesign name="github" size={50} color="#3EC6FF" />
              <Text>@muklhis-h</Text>
            </View>
          </View>
        </View>
        <View style={styles.bodydown}>
          <Text>Hubungi Saya</Text>
          <View>
          <View style={styles.contentDown}>
            <AntDesign name="facebook-square" size={40} color="#3EC6FF" />
            <Text style={styles.textContent}>muklhis.hanafi</Text>
          </View>
          <View style={styles.contentDown}>
            <AntDesign name="instagram" size={40} color="#3EC6FF" style={{marginRight:5}} />
            <Text style={styles.textContent}>@muklhis_hanafi</Text>
          </View>
          <View style={styles.contentDown}>
            <AntDesign name="twitter" size={40} color="#3EC6FF" style={{marginRight:10}} />
            <Text style={styles.textContent}>@muklhis</Text>
          </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1
  },
  bodyup:{
    justifyContent:'center',
    alignItems:'center',
    paddingTop: 60
  },
  textup:{
    fontSize: 30,
    fontWeight:'bold',
    color: '#003366'
  },
  textname:{
    fontSize:20,
    fontWeight:'bold',
    color:'#003366'
  },
  textabout:{
    color:'#3EC6FF',
    fontWeight:'bold'
  },
  bodymid:{
    backgroundColor:'#EFEFEF',
    borderRadius:10,
    padding:15,
    marginHorizontal:15,
    marginTop:20
  },
  giticon:{
    flexDirection:'row',
    justifyContent:'space-around'
  },
  bodydown:{
    backgroundColor:'#EFEFEF',
    borderRadius:10,
    padding:15,
    marginHorizontal:15,
    marginTop:15
  },
  contentDown:{
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center'
  },
  textContent:{
    paddingLeft:5
  }
})
