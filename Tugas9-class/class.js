// Release 0

class Animal {
  constructor(name){
    this.name = name;
    this.legs = 4;
    this.cold_blooded = false;
  }
  nama_animal(){
    return 'nama animal = '+ this.name;
  }
}


var sheep = new Animal("shaun");

console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)

// Release 1

class Ape extends Animal{
  constructor(name,mod){
    super(name);
    this.model = mod;
  }
  yell(){
    return 'auooo';
  }
}

var sungokong = new Ape("Kera Sakti")
console.log(sungokong.yell());

class Frog extends Animal{
  constructor(name,mod){
    super(name);
    this.model = mod;
  }
  jump(){
    return 'hop hop';
  }
}

var kodok = new Frog("buduk")
console.log(kodok.jump()); // "hop hop"

//2. Function to Class

/* function Clock({ template }) {

  var timer;

  function render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  this.stop = function() {
    clearInterval(timer);
  };

  this.start = function() {
    render();
    timer = setInterval(render, 1000);
  };

}

var clock = new Clock({template: 'h:m:s'});
clock.start();

*/

// rubah
class Clock {
  constructor({template}){
    this.template= template;
  }
  render() {
    //var timer;
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = this.template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }
  stop() {
    clearInterval(this.timer);
  }
  start() {
    this.render();
    this.timer = setInterval(this.render.bind(this), 1000);
  }
}


var clock = new Clock({template: 'h:m:s'});
clock.start();
