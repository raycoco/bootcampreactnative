
var nama = 'John'
var peran = ''

if (nama == "" && peran == "") {
  console.log("Nama harus diisi!");
} else if (nama == "John" || peran == "") {
  console.log("Halo John, Pilih peranmu untuk memulai game!");
} else if (nama == "Jane" || peran == "Penyihir") {
  console.log("Selamat datang di Dunia Werewolf, Jane");
  console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!");
} else if (nama == "Jenita" || peran == "Guard") {
  console.log("Selamat datang di Dunia Werewolf, Jenita");
  console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.");
} else if (nama == "Junaidi" || peran == "Warewolf") {
  console.log("Selamat datang di Dunia Werewolf, Junaidi");
  console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!");
} else {
  console.log("Siapa kamu?");
}

//  Switch case
var hari = 21;
var bulan = 1;
var tahun = 1945;

if (hari < 1) {
  console.log('Tanggal tidak boleh 0, harus antara 1 - 31');
} if (hari > 31) {
  console.log('Tanggal harus antara 1 - 31, tidak boleh lebih dari 31');
} else {
  switch(hari) {
    case 21 : { console.log('21'); break;}
    Default : {console.log('Tanggal harus antara 1 - 31');}
  }
}

if (bulan < 1) {
  console.log("bulan harus antara 1 - 12");
} if (bulan > 12) {
  console.log("bulan tidak lebih dari 12, harus antara 1 - 12");
} else {
switch(bulan) {
  case 1:   { console.log('Januari'); break; }
  case 2:   { console.log('Februari'); break; }
  case 3:   { console.log('Maret'); break; }
  case 4:   { console.log('April'); break; }
  case 5:  { console.log('Mei'); break; }
  case 6:   { console.log('Juni'); break; }
  case 7:   { console.log('Juli'); break; }
  case 8:   { console.log('Agustus'); break; }
  case 9:   { console.log('September'); break; }
  case 10:  { console.log('oktober'); break; }
  case 11:   { console.log('November'); break; }
  case 12:  { console.log('Desember'); break; }
  Default: { console.log('Bulan Salah');}
}
}

if (tahun < 1900 ){
  console.log('Tahun dibawah 1900, harus antara 1900 - 2200');
} if (tahun > 2200){
  console.log('Tahun diatas 2200, harus antara 1900 - 2200');
} else {
  switch(tahun) {
    case 1945 : { console.log('1945'); break;}
    Default : {console.log('Tahun harus antara 1900 - 2200');}}
}

if (hari = 21) {
  if (tahun = 1945) {
    if (bulan = 1) {
  switch(bulan) {
    case 1:   { console.log(hari+' Januari '+tahun); break; }
    case 2:   { console.log(hari+' Februari '+tahun); break; }
    case 3:   { console.log(hari+' Maret '+tahun); break; }
    case 4:   { console.log(hari+' April '+tahun); break; }
    case 5:  { console.log(hari+' Mei '+tahun); break; }
    case 6:   { console.log(hari+' Juni '+tahun); break; }
    case 7:   { console.log(hari+' Juli '+tahun); break; }
    case 8:   { console.log(hari+' Agustus '+tahun); break; }
    case 9:   { console.log(hari+' September '+tahun); break; }
    case 10:  { console.log(hari+' oktober '+tahun); break; }
    case 11:   { console.log(hari+' November '+tahun); break; }
    case 12:  { console.log(hari+' Desember '+tahun); break; }
    Default: { console.log('Bulan Salah');}
  }
}
}
} else {
  console.log("masukan tanggal yang benar");
}
